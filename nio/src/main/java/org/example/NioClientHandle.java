package org.example;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Set;

//客户端处理器
public class NioClientHandle implements Runnable {
    private String host;
    private int port;
    private volatile boolean started;
    private Selector selector;
    private SocketChannel socketChannel;

    public NioClientHandle(String ip, int port) {
        this.host = ip;
        this.port = port;

        try {
            //创建选择器实例
            selector = Selector.open();
            //创建SocketChannel实例
            socketChannel = SocketChannel.open();
            //设置通道为非阻塞
            socketChannel.configureBlocking(false);
            started = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            doConnect();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        //循环遍历selector
        while (started) {
            try {
                //如果没有读写事件，selector会阻塞，但是1s后会被唤醒
                selector.select(1000);
                //获取当前事件的集合
                Set<SelectionKey> selectionKeys = selector.selectedKeys();
                Iterator<SelectionKey> iterator = selectionKeys.iterator();
                SelectionKey key = null;
                while (iterator.hasNext()) {
                    key = iterator.next();
                    try {
                        handleInput(key);
                        iterator.remove();
                    } catch (Exception e) {
                        if (key != null) {
                            key.cancel();
                            if (key.channel() != null) {
                                key.channel().close();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
        //selector关闭后会自动释放里面管理的资源
        if (selector != null) {
            try {
                selector.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //具体的事件处理方法
    private void handleInput(SelectionKey key) throws IOException {
        if (key.isValid()) {
            //获取关心当前事件的channel
            SocketChannel sc = (SocketChannel) key.channel();
            //连接事件
            if (key.isConnectable()) {
                if (sc.finishConnect()) {
                    sc.register(selector, SelectionKey.OP_READ);
                } else {
                    System.exit(1);
                }
            }
            //有数据可读事件
            if (key.isReadable()) {
                //创建一个bytebuffer，开辟一个1M的缓冲区
                ByteBuffer buffer = ByteBuffer.allocate(1024);
                //从通道中读数据，然后写入buffer
                int readBytes = sc.read(buffer);
                //读取到数据
                if (readBytes > 0) {
                    //buffer切换为读模式，将buffer当前的limit设置为position，position=0，用于后续对buffer的读操作
                    buffer.flip();
                    //根据buffer可读字节数创建字节数组
                    byte[] bytes = new byte[buffer.remaining()];
                    //将buffer可读字节数组复制到新建的数组中
                    buffer.get(bytes);
                    String message = new String(bytes, StandardCharsets.UTF_8);
                    System.out.println("客户器接收到的消息：" + message);
                }
                //读取数据结束
                else if (readBytes < 0) {
                    //取消特定的注册关系
                    key.cancel();
                    //关闭通道
                    sc.close();
                }
            }
        }
    }

    //发送应答消息
    private void doWrite(SocketChannel channel, String request)
            throws IOException {
        //将消息编码为字节数组
        byte[] bytes = request.getBytes();
        //根据数组容量创建ByteBuffer
        ByteBuffer buffer = ByteBuffer.allocate(bytes.length);
        //将字节数组复制到缓冲区
        buffer.put(bytes);
        //写切换为读
        buffer.flip();
        //channel读取缓冲区的字节数组
        channel.write(buffer);
    }

    private void doConnect() throws IOException {
        //如果连接成功就注册读事件
        if (socketChannel.connect(new InetSocketAddress(host, port))) {
            socketChannel.register(selector, SelectionKey.OP_READ);
        }
        //连接失败就注册连接事件
        else {
            socketChannel.register(selector, SelectionKey.OP_CONNECT);
        }
    }

    //写数据对外暴露的API
    public void sendMsg(String msg) throws Exception {
        doWrite(socketChannel, msg);
    }


}
