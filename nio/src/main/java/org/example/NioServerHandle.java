package org.example;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

//nio服务端处理器
public class NioServerHandle implements Runnable {
    private volatile boolean start;
    private ServerSocketChannel serverSocketChannel;
    private Selector selector;

    //构造方法，指定要监听的端口号
    public NioServerHandle(int port) {
        try {
            //创建选择器实例
            selector = Selector.open();
            //创建ServerSocketChannel实例
            serverSocketChannel = ServerSocketChannel.open();

            //设置false,通道为非阻塞；如果设置为true,为阻塞模式，但是启动会报错
            serverSocketChannel.configureBlocking(false);
            //绑定端口号
            serverSocketChannel.socket().bind(new InetSocketAddress(port));
            //注册事件，关心客户端连接事件
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
            start = true;
            System.out.println("服务端已启动，端口号：" + port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //处理事件
    private void handleInput(SelectionKey key) throws IOException {
        if (key.isValid()) {
            //处理新接入的客户端请求事件
            if (key.isAcceptable()) {
                //获取关心当前事件的channel
                ServerSocketChannel ssc = (ServerSocketChannel) key.channel();
                //接受连接
                SocketChannel accept = ssc.accept();
                System.out.println("建立连接");
                accept.configureBlocking(false);
                //关注读事件
                accept.register(selector, SelectionKey.OP_READ);
            }
            //处理客户端发送的数据，读事件
            if (key.isReadable()) {
                SocketChannel sc = (SocketChannel) key.channel();
                //创建一个bytebuffer，开辟一个缓冲区
                ByteBuffer buffer = ByteBuffer.allocate(1024);
                //从通道中读数据，然后写入buffer
                int readBytes = sc.read(buffer);
                //读取到数据
                if (readBytes > 0) {
                    //buffer切换为读模式，将buffer当前的limit设置为position，position=0，用于后续对buffer的读操作
                    buffer.flip();
                    //根据buffer可读字节数创建字节数组
                    byte[] bytes = new byte[buffer.remaining()];
                    //将buffer可读字节数组复制到新建的数组中
                    buffer.get(bytes);
                    String message = new String(bytes, StandardCharsets.UTF_8);
                    System.out.println("服务器接收到的消息：" + message);
                    String result = "Hello " + message + ", now is " + new Date();
                    //发送应答消息
                    doWrite(sc, result);
                }
                //读取数据结束
                else if (readBytes < 0) {
                    //取消特定的注册关系
                    key.cancel();
                    //关闭通道
                    sc.close();
                }
            }
        }
    }

    //发送应答消息
    private void doWrite(SocketChannel socketChannel, String response) throws IOException {
        //将消息编码为字节数组
        byte[] bytes = response.getBytes();
        //根据数组容量创建ByteBuffer
        ByteBuffer buffer = ByteBuffer.allocate(bytes.length);
        //将字节数组复制到缓冲区
        buffer.put(bytes);
        //写切换为读
        buffer.flip();
        //channel读取缓冲区的字节数组
        socketChannel.write(buffer);
    }

    @Override
    public void run() {
        while (start) {
            try {
                //如果没有读写事件，selector会阻塞，但是1s后会被唤醒
                selector.select(1000);
                //获取当前事件的集合
                Set<SelectionKey> selectionKeys = selector.selectedKeys();
                Iterator<SelectionKey> iterator = selectionKeys.iterator();
                while (iterator.hasNext()) {
                    SelectionKey selectionKey = iterator.next();
                    //删除已处理过的SelectionKey，否则这个key还是一个激活状态，下次循环还会再次处理这个key
                    handleInput(selectionKey);
                    iterator.remove();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void stop() {
        start = false;
    }
}
