package org.example;

import java.util.Scanner;

public class NioClient {
    private static NioClientHandle nioClientHandle;

    public static void start() {
        nioClientHandle = new NioClientHandle("127.0.0.1", 9001);
        new Thread(nioClientHandle, "Server").start();
    }

    //向服务器发送消息
    public static boolean sendMsg(String msg) throws Exception {
        nioClientHandle.sendMsg(msg);
        return true;
    }

    public static void main(String[] args) throws Exception {
        start();
        Scanner scanner = new Scanner(System.in);
        while (NioClient.sendMsg(scanner.next())) ;
    }

}
