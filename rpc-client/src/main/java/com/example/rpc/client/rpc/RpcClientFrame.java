package com.example.rpc.client.rpc;

import com.example.rpc.remote.vo.RegisterVO;
import org.springframework.stereotype.Service;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

//客户端代理部分
@Service
public class RpcClientFrame {
    //获得服务提供方的地址列表
    private static List<InetSocketAddress> getAddressList(String serviceName) throws Exception {
        Socket socket = new Socket();
        socket.connect(new InetSocketAddress("127.0.0.1", 9001));
        try (ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
             ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream())) {
            //查询服务列表
            outputStream.writeBoolean(true);
            //发送服务名称
            outputStream.writeUTF(serviceName);
            outputStream.flush();
            Set<RegisterVO> voSet = (Set<RegisterVO>) inputStream.readObject();
            List<InetSocketAddress> services = new ArrayList<>();
            for (RegisterVO registerVO : voSet) {
                String host = registerVO.getHost();
                int port = registerVO.getPort();
                InetSocketAddress address = new InetSocketAddress(host, port);
                services.add(address);
            }
            System.out.println("查询服务列表成功：" + serviceName);
            return services;

        } finally {
            if (socket != null) {
                socket.close();
            }
        }
    }

    //获取的远程服务的地址
    private static InetSocketAddress getService(String serviceName) throws Exception {
        Random random = new Random();
        //获得服务提供方的地址列表
        List<InetSocketAddress> addressList = getAddressList(serviceName);
        //随机选一台
        InetSocketAddress address = addressList.get(random.nextInt(addressList.size()));
        System.out.println("本次调用选择的服务器是：" + address);
        return address;
    }

    //动态代理，实现对远程服务的访问
    private static class DynProxy implements InvocationHandler {
        private Class<?> serviceInterface;
        private InetSocketAddress socketAddress;

        public DynProxy(Class<?> serviceInterface, InetSocketAddress socketAddress) {
            this.serviceInterface = serviceInterface;
            this.socketAddress = socketAddress;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            Socket socket = new Socket();
            socket.connect(socketAddress);
            try (ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                 ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream())) {
                //方法所在类的名字
                outputStream.writeUTF(serviceInterface.getName());
                //方法名
                outputStream.writeUTF(method.getName());
                //方法入参类型
                outputStream.writeObject(method.getParameterTypes());
                //方法入参
                outputStream.writeObject(args);
                outputStream.flush();
                //接收服务器的输出
                System.out.println(serviceInterface + "remote exec success !");
                return inputStream.readObject();
            } finally {
                if (socket != null) {
                    socket.close();
                }
            }
        }
    }

    //远程服务的代理对象，参数为客户端要调用的服务
    public static Object getRemoteProxyObject(Class serviceInterface) throws Exception {
        //获得远程服务的一个网络地址
        InetSocketAddress address = getService(serviceInterface.getName());
        //拿到一个代理对象，由代理对象通过网络进行实际的服务调用
        return Proxy.newProxyInstance(serviceInterface.getClassLoader(),
                new Class[]{serviceInterface}, new DynProxy(serviceInterface, address));
    }

}
