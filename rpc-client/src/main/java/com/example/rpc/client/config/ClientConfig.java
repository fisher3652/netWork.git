package com.example.rpc.client.config;

import com.example.rpc.client.rpc.RpcClientFrame;
import com.example.rpc.remote.SendSms;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ClientConfig {

    @Autowired
    private RpcClientFrame rpcClientFrame;

    //注入远端服务的SendSms类，可以在controller中使用其中的方法
    @Bean
    public SendSms getSendSms() throws Exception {
        return (SendSms) rpcClientFrame.getRemoteProxyObject(SendSms.class);
    }
}
