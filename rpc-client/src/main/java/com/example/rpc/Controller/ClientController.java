package com.example.rpc.Controller;

import com.example.rpc.remote.SendSms;
import com.example.rpc.remote.vo.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientController {

    @Autowired
    private SendSms sendSms;

    @RequestMapping(value = "sendMsg")
    public void sendMsg() {
        long start = System.currentTimeMillis();
        UserInfo userInfo = new UserInfo("fisher", "123456");
        System.out.println("send message: " + sendSms.sendMsg(userInfo));
        System.out.println("共耗时：" + (System.currentTimeMillis() - start) + "ms");
    }
}
