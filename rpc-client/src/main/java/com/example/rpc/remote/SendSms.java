package com.example.rpc.remote;


import com.example.rpc.remote.vo.UserInfo;

public interface SendSms {

    //发送短信
    boolean sendMsg(UserInfo userInfo);

}
