package com.example.rpc.sms;

import com.example.rpc.remote.SendSms;
import com.example.rpc.remote.vo.UserInfo;
import org.springframework.stereotype.Service;

//短信息发送服务的实现
@Service
public class SendSmsImpl implements SendSms {
    @Override
    public boolean sendMsg(UserInfo userInfo) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("已发送短信到：" + userInfo.getName());
        return true;
    }
}
