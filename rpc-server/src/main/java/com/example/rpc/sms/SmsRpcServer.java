package com.example.rpc.sms;

import com.example.rpc.remote.SendSms;
import com.example.rpc.rpc.base.RpcServerFrame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Random;

//rpc的服务端，提供服务
@Service
public class SmsRpcServer {
    @Autowired
    private RpcServerFrame rpcServerFrame;

    @PostConstruct
    public void server() {
        Random random = new Random();
        int port = 8888 + random.nextInt(100);
        try {
            //往远程服务器注册本服务，并提供业务类
            rpcServerFrame.startService(SendSms.class.getName(), "127.0.0.1", port, SendSmsImpl.class);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

}
