package com.example.rpc.rpc.base;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//服务端
@Service
public class RpcServerFrame {

    private static ExecutorService executorService =
            Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    @Autowired
    private RegisterServiceWithRegCenter registerServiceWithRegCenter;

    //处理服务请求任务
    private static class ServerTask implements Runnable {

        private Socket socket;

        private RegisterServiceWithRegCenter registerServiceWithRegCenter;

        public ServerTask(Socket socket, RegisterServiceWithRegCenter registerServiceWithRegCenter) {
            this.socket = socket;
            this.registerServiceWithRegCenter = registerServiceWithRegCenter;
        }

        @Override
        public void run() {
            try (ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                 ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream())) {
                //方法所在的类名
                String serviceName = inputStream.readUTF();
                //方法名
                String methodName = inputStream.readUTF();
                //方法的入参类型
                Class<?>[] paramTypes = (Class<?>[]) inputStream.readObject();
                //方法的入参值
                Object[] args = (Object[]) inputStream.readObject();

                //从容器中拿到服务的class对象
                Class serviceClass = registerServiceWithRegCenter.getLocalService(serviceName);
                if (serviceClass == null) {
                    throw new ClassNotFoundException(serviceName + "not found");
                }
                //通过反射，执行实际的方法
                Method method = serviceClass.getMethod(methodName, paramTypes);
                Object result = method.invoke(serviceClass.newInstance(), args);
                //将服务的执行结果返回给调用者
                outputStream.writeObject(result);
                outputStream.flush();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void startService(String serverName, String host, int port, Class impl) throws Throwable {
        ServerSocket serverSocket = new ServerSocket();
        serverSocket.bind(new InetSocketAddress(port));
        System.out.println("server is on :" + port);
        registerServiceWithRegCenter.regRemote(serverName, host, port, impl);
        try {
            while (true) {
                executorService.execute(new ServerTask(serverSocket.accept(), registerServiceWithRegCenter));
            }
        } finally {
            serverSocket.close();
        }
    }

}
