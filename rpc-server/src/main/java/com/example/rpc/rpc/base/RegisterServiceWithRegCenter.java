package com.example.rpc.rpc.base;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//注册服务
@Service
public class RegisterServiceWithRegCenter {

    //本地可提供服务的一个名单，用缓存实现
    private static final Map<String, Class> serviceCache = new ConcurrentHashMap<>();

    //往远程服务器注册本服务，同时在本地注册本服务
    public void regRemote(String serverName, String host, int port, Class impl) throws IOException {
        //登记到注册中心
        Socket socket = new Socket();
        //服务端的通信地址
        InetSocketAddress inetSocketAddress = new InetSocketAddress("127.0.0.1", 9001);
        //连接服务器
        socket.connect(inetSocketAddress);
        //实例化与客户端通信的输入输出流，先输出再输入
        try (ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
             ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream())) {
            //用false表示注册服务，true表示查询服务
            outputStream.writeBoolean(false);
            //提供服务名
            outputStream.writeUTF(serverName);
            //提供ip
            outputStream.writeUTF(host);
            //提供端口
            outputStream.writeInt(port);
            outputStream.flush();
            //接收服务端的输出
            if (inputStream.readBoolean()) {
                System.out.println(serverName + "服务注册成功");
            }
            //可提供的服务放入本地缓存
            serviceCache.put(serverName, impl);
        } finally {
            if (socket != null) {
                socket.close();
            }
        }
    }

    //获取服务
    public Class getLocalService(String serviceName) {
        return serviceCache.get(serviceName);
    }

}
