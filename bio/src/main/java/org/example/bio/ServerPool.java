package org.example.bio;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServerPool {

    private static ExecutorService executorService =
            Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    public static void main(String[] args) throws IOException {
        //服务端启动必备
        ServerSocket serverSocket = new ServerSocket();
        //绑定监听的端口
        serverSocket.bind(new InetSocketAddress(9001));
        System.out.println("Server is started ...");
        try {
            while (true) {
                executorService.execute(new ServerTask(serverSocket.accept()));
            }
        } finally {
            serverSocket.close();
        }
    }

    public static class ServerTask implements Runnable {
        Socket socket = null;

        public ServerTask(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            System.out.println("connect success ...");
            //实例化与客户端通信的输入输出流，先输入再输出
            try(ObjectInputStream inputStream=new ObjectInputStream(socket.getInputStream());
                ObjectOutputStream outputStream=new ObjectOutputStream(socket.getOutputStream())){
                //接收客户端的输出，也就是服务端的输入
                System.out.println("wait message ...");
                String readUTF = inputStream.readUTF();
                System.out.println("receive :" + readUTF);
                //服务端输出，也就是客户端输入
                outputStream.writeUTF("Hello, " + readUTF);
                outputStream.flush();

            }catch (Exception e){
                e.printStackTrace();
            }finally {
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
