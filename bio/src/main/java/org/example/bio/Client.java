package org.example.bio;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws IOException {
        //客户端必备
        Socket socket = new Socket();
        //服务端的通信地址
        InetSocketAddress inetSocketAddress = new InetSocketAddress("127.0.0.1", 9001);
        //连接服务器
        socket.connect(inetSocketAddress);
        //实例化与服务端通信的输入输出流，先输出再输入
        try(ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
        ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream())) {
            //向服务端输出
            System.out.print("请输入需要发送的消息：");
            Scanner scanner = new Scanner(System.in);
            outputStream.writeUTF(scanner.next());
            outputStream.flush();
            //接收服务端的输出
            System.out.println(inputStream.readUTF());
        } finally {
            if (socket != null) {
                socket.close();
            }
        }
    }
}
