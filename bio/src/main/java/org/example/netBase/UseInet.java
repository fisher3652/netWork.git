package org.example.netBase;

import java.io.IOException;
import java.net.InetAddress;

public class UseInet {
    public static void main(String[] args) throws IOException {
        InetAddress address = InetAddress.getByName("www.baidu.com");
        System.out.println(address);
        System.out.println(address.isReachable(100));
        InetAddress address2 = InetAddress.getByName("180.101.49.12");
        System.out.println(address2.getHostName());
        InetAddress[] allByName = InetAddress.getAllByName("www.baidu.com");
        for(InetAddress addr:allByName ){
            System.out.println(addr);
        }
        byte[] bytes = {(byte)192,(byte)168,1,1};
        InetAddress byAddress = InetAddress.getByAddress(bytes);
        System.out.println(byAddress);
    }

}
