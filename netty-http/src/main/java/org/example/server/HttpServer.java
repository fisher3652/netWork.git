package org.example.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpContentCompressor;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.SelfSignedCertificate;

public class HttpServer {
    //设置服务端端口
    public static final int port = 6789;
    //通过nio方式来接收连接和处理连接
    private static EventLoopGroup group = new NioEventLoopGroup();
    //服务端必备
    private static ServerBootstrap b = new ServerBootstrap();
    //是否开启SSL模式(https)
    private static final boolean SSL = false;

    public static void main(String[] args) throws Exception {
        final SslContext sslCtx;
        if (SSL) {
            SelfSignedCertificate ssc = new SelfSignedCertificate();
            //设置签名证书
            sslCtx = SslContextBuilder.forServer(ssc.certificate(), ssc.privateKey()).build();
        } else {
            sslCtx = null;
        }
        try {
            b.group(group).channel(NioServerSocketChannel.class).childHandler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    ChannelPipeline pipeline = socketChannel.pipeline();
                    if (sslCtx != null) {
                        pipeline.addLast(sslCtx.newHandler(socketChannel.alloc()));
                    }
                    //入站，对请求报文解码，用于后续业务处理
                    pipeline.addLast("decode", new HttpRequestDecoder());
                    //出站，对应答报文编码
                    pipeline.addLast("encode", new HttpResponseEncoder());
                    //聚合http为一个完整的报文，最大不超过10M
                    pipeline.addLast("aggregator", new HttpObjectAggregator(10 * 1024 * 1024));
                    //对应答报文压缩
                    pipeline.addLast("compressor", new HttpContentCompressor());
                    //业务处理
                    pipeline.addLast(new BusiHandler());
                }
            });
            //服务器绑定端口监听
            ChannelFuture future = b.bind(port).sync();
            System.out.println("服务端启动成功,端口是:" + port);
            //监听服务器关闭监听
            future.channel().closeFuture().sync();
        } finally {
            group.shutdownGracefully().sync();
        }
    }

}
