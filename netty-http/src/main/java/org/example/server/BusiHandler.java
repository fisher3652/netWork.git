package org.example.server;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;

import java.util.Date;

public class BusiHandler extends ChannelInboundHandlerAdapter {
    //建立连接时，打印客户端地址
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("连接的客户端地址:" + ctx.channel().remoteAddress());
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        String result = "";
        //拿到请求对象
        FullHttpRequest httpRequest = (FullHttpRequest) msg;
        try {
            //获取路径
            String path = httpRequest.uri();
            //获取body
            String body = httpRequest.content().toString(CharsetUtil.UTF_8);
            //获取请求方法
            HttpMethod method = httpRequest.method();
            System.out.println("接收到:" + method + " 请求");
            //如果不是这个路径，就直接返回错误
            if (!"/test".equalsIgnoreCase(path)) {
                result = "非法请求!" + path;
                //返回400
                send(ctx, result, HttpResponseStatus.BAD_REQUEST);
                return;
            }
            //这里只处理GET请求
            if (HttpMethod.GET.equals(method)) {
                //接受到的消息，做业务逻辑处理...
                System.out.println("接收到的消息体:" + body);
                result = "GET请求,应答:现在时间是" + new Date();
                //返回200
                send(ctx, result, HttpResponseStatus.OK);
                return;
            }
        } finally {
            //释放请求
            httpRequest.release();
        }
    }

    private void send(ChannelHandlerContext ctx, String context, HttpResponseStatus status) {
        //创建相应对象，设置http版本，状态码，应答内容
        DefaultFullHttpResponse response = new DefaultFullHttpResponse(
                HttpVersion.HTTP_1_1, status, Unpooled.copiedBuffer(context, CharsetUtil.UTF_8));
        //设置content-type格式
        response.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/plain;charset=UTF-8");
        //写完数据就关闭chanel
        ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }

}
