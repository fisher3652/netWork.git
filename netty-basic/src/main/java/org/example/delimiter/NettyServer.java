package org.example.delimiter;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.LineBasedFrameDecoder;

import java.net.InetSocketAddress;

//基于Netty的服务端
public final class NettyServer {
    private final int port;

    public NettyServer(int port) {
        this.port = port;
    }

    public void start() throws InterruptedException {
        //线程组
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            //服务端启动必备
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(group)
                    .channel(NioServerSocketChannel.class)//指定使用nio的通信模式
                    .localAddress(new InetSocketAddress(port))//指定端口监听
                    .childHandler(new ChannelInitializer<Channel>() {
                        @Override
                        protected void initChannel(Channel channel) throws Exception {
                            ByteBuf buf = Unpooled.copiedBuffer("!@#".getBytes());
                            //添加分隔符解码器
                            channel.pipeline().addLast(new DelimiterBasedFrameDecoder(1024, buf));
                            channel.pipeline().addLast(new NettyServerHandler());//添加handler
                        }

                    });
            //异步绑定到服务器，sync()会阻塞到任务完成
            ChannelFuture future = bootstrap.bind().sync();
            //阻塞当前线程，直到服务器的ServerChannel被关闭
            future.channel().closeFuture().sync();
        } finally {
            group.shutdownGracefully().sync();
        }
    }

    public static void main(String[] args) {
        NettyServer server = new NettyServer(9001);
        System.out.println("服务器即将启动");
        try {
            server.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("服务器关闭");
    }
}
