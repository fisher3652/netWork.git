package org.example.delimiter;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.LineBasedFrameDecoder;

import java.net.InetSocketAddress;

public class NettyClient {
    private String host;
    private int port;

    public NettyClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void start() throws InterruptedException {
        //线程组
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            //客户端启动必备
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group)
                    .channel(NioSocketChannel.class)//指定使用nio的通信模式
                    .remoteAddress(new InetSocketAddress(host, port))//指定服务器的ip和端口
                    .handler(new ChannelInitializer<Channel>() {
                        @Override
                        protected void initChannel(Channel channel) throws Exception {
                            ByteBuf buf = Unpooled.copiedBuffer("!@#".getBytes());
                            //添加分隔符解码器
                            channel.pipeline().addLast(new DelimiterBasedFrameDecoder(1024, buf));
                            channel.pipeline().addLast(new NettyClientHandler());//添加handler
                        }
                    });
            //异步连接到服务器，sync()会阻塞到任务完成
            ChannelFuture future = bootstrap.connect().sync();
            //阻塞当前线程，直到服务器的ServerChannel被关闭
            future.channel().closeFuture().sync();
        } finally {
            group.shutdownGracefully().sync();
        }
    }

    public static void main(String[] args) {
        NettyClient client = new NettyClient("127.0.0.1", 9001);
        try {
            client.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
