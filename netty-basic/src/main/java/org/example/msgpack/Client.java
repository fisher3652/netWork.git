package org.example.msgpack;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.LineBasedFrameDecoder;

import java.net.InetSocketAddress;

public class Client {

    public static void main(String[] args) throws InterruptedException {
        //线程组
        EventLoopGroup group = new NioEventLoopGroup();
        //客户端必备
        Bootstrap b = new Bootstrap();
        try {
            b.group(group)//将线程组传入
                    .channel(NioSocketChannel.class)//指定使用NIO进行网络传输
                    .remoteAddress(new InetSocketAddress("127.0.0.1", 9001))//配置要连接服务器的ip地址和端口
                    .handler(new ChannelInitializer<Channel>() {
                        @Override
                        protected void initChannel(Channel channel) throws Exception {
                            //通过LengthFieldPrepender计算报文长度，把这个值存入报文头，并指定大小2字节，2的16次方，即长度不能超过65536
                            channel.pipeline().addLast(new LengthFieldPrepender(2));
                            //对服务器应答报文解码，解决粘包半包，服务端的应答报文这里使用回车换行符进行分隔
                            channel.pipeline().addLast(new LineBasedFrameDecoder(1024));
                            //对发送的数据做编码-序列化
                            channel.pipeline().addLast(new MsgPackEncoder());
                            channel.pipeline().addLast(new ClientHandler());
                        }
                    });
            ChannelFuture future = b.connect().sync();
            System.out.println("已连接到服务器。。。");
            future.channel().closeFuture().sync();
        } finally {
            //优雅关闭线程组
            group.shutdownGracefully().sync();
        }


    }


}
