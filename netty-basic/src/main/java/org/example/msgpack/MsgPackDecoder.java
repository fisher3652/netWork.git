package org.example.msgpack;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import org.msgpack.MessagePack;

import java.util.List;

//基于MessagePack的解码器，反序列化
public class MsgPackDecoder extends MessageToMessageDecoder<ByteBuf> {
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        //获取消息长度
        int length = byteBuf.readableBytes();
        //创建字节数据，并定义长度
        byte[] bytes = new byte[length];
        //将byteBuf转换为字节数组
        byteBuf.getBytes(byteBuf.readerIndex(), bytes);
        MessagePack messagePack = new MessagePack();
        //将字节数组解码成User对象，并添加到解码器集合中
        list.add(messagePack.read(bytes, User.class));
    }
}
