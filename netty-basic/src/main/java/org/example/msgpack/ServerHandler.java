package org.example.msgpack;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.concurrent.atomic.AtomicInteger;

public class ServerHandler extends ChannelInboundHandlerAdapter {
    private AtomicInteger counter = new AtomicInteger(0);

    //服务端读取到网络数据后的处理
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        //将上一个handler解码后的数据强转
        User user = (User) msg;
        System.out.println("服务端接收的数据:" + user + ", count = " + counter.incrementAndGet());
        //服务器应答消息，使用回车换行符分隔
        String resp = "user :" + user.getUserName() + System.getProperty("line.separator");
        ctx.writeAndFlush(Unpooled.copiedBuffer(resp.getBytes()));
        //传递消息至下一个处理器
        ctx.fireChannelRead(user);
    }
}
