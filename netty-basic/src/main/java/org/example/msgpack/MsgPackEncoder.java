package org.example.msgpack;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.msgpack.MessagePack;

//基于MessagePack的编码器，序列化
public class MsgPackEncoder extends MessageToByteEncoder<User> {
    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, User user, ByteBuf byteBuf) throws Exception {
        MessagePack messagePack = new MessagePack();
        //将User对象编码成字节数组
        byte[] write = messagePack.write(user);
        //将字节数组写入ByteBuf
        byteBuf.writeBytes(write);
    }
}
