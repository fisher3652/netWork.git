package org.example.msgpack;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

import java.net.InetSocketAddress;

public class Server {

    public static void main(String[] args) throws InterruptedException {
        EventLoopGroup group = new NioEventLoopGroup();
        //服务端启动必须
        ServerBootstrap b = new ServerBootstrap();
        try {
            b.group(group).channel(NioServerSocketChannel.class).localAddress(new InetSocketAddress(9001))
                    .childHandler(new ChannelInitializer<Channel>() {
                        @Override
                        protected void initChannel(Channel channel) throws Exception {
                            //最大长度65536，偏移量0，长度值长度2字节，长度修正值0，丢弃2字节（把存放长度值的2字节消息头丢弃）
                            channel.pipeline().addLast(new LengthFieldBasedFrameDecoder(65536,
                                    0, 2, 0, 2));
                            //解码
                            channel.pipeline().addLast(new MsgPackDecoder());
                            channel.pipeline().addLast(new ServerHandler());
                        }
                    });
            ChannelFuture f = b.bind().sync();
            System.out.println("服务器启动完成，等待客户端的连接和数据.....");
            f.channel().closeFuture().sync();
        } finally {
            //优雅关闭线程组
            group.shutdownGracefully().sync();
        }
    }
}
