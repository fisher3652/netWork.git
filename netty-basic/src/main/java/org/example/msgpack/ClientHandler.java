package org.example.msgpack;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;

import java.util.concurrent.atomic.AtomicInteger;

public class ClientHandler extends SimpleChannelInboundHandler<ByteBuf> {
    private AtomicInteger counter = new AtomicInteger(0);

    //客户端读取到网络数据后的处理
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf) throws Exception {
        System.out.println("客户端接收的消息：" + byteBuf.toString(CharsetUtil.UTF_8) + ", count = " + counter.incrementAndGet());
    }

    //channel活跃时，做业务处理
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        //分5次写入，一起刷新
        for (int i = 0; i < 5; i++) {
            User user = new User();
            user.setUserName("user" + i);
            user.setAge(i);
            System.out.println("发送：" + user);
            ctx.write(user);
        }
        ctx.flush();
    }
}
