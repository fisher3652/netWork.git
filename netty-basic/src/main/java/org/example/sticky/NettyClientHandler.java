package org.example.sticky;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;

import java.util.concurrent.atomic.AtomicInteger;

public class NettyClientHandler extends SimpleChannelInboundHandler<ByteBuf> {

    private AtomicInteger count = new AtomicInteger(0);

    //channel活跃后进行业务处理
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        for (int i = 0; i < 200; i++) {
            //line.separator换行符，分200次发送到服务端
            ctx.writeAndFlush(Unpooled.copiedBuffer("fisher" +
                    System.getProperty("line.separator"), CharsetUtil.UTF_8));
        }
    }

    //读取到网络数据后进行业务处理
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf) throws Exception {
        //读取到服务端响应的数据进行打印
        System.out.println("Client accept:" + byteBuf.toString(CharsetUtil.UTF_8) +
                ", count=" + count.incrementAndGet());
    }
}
