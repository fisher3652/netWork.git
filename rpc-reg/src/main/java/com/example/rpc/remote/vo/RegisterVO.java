package com.example.rpc.remote.vo;

import java.io.Serializable;

//注册中心实体
public class RegisterVO implements Serializable {
    private String host;
    private int port;

    public RegisterVO(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
