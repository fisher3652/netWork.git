package com.example.rpc.service;

import com.example.rpc.remote.vo.RegisterVO;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//注册中心，服务端启动时在注册中心注册自己的信息，客户端在启动时查询服务端信息
@Service
public class RegisterCenter {

    private static ExecutorService executorService =
            Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    //key表示服务名，value表示服务地址的集合
    private static final Map<String, Set<RegisterVO>> serviceHoler = new HashMap<>();

    //注册服务的端口号
    private int port;

    //服务注册
    private static synchronized void registerService(String serviceName, String host, int port) {
        //获得当前服务的已有地址集合
        Set<RegisterVO> voSet = serviceHoler.get(serviceName);
        if (voSet == null) {
            //已有地址集合为空，则新增集合
            voSet = new HashSet<>();
            serviceHoler.put(serviceName, voSet);
            System.out.println("服务注册成功，" + serviceName + "," + host + ":" + port);
        }
        //把新加入的服务地址加入集合
        voSet.add(new RegisterVO(host, port));
        System.out.println("服务地址添加成功," + host + ":" + port);
    }

    //取出服务提供者
    private static Set<RegisterVO> getService(String serviceName) {
        return serviceHoler.get(serviceName);
    }

    //处理服务请求的任务，1、注册服务，2、查询服务
    private static class ServerTask implements Runnable {
        private Socket socket = null;

        public ServerTask(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try (ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                 ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream())) {
                //检查当前请求是注册还是查询
                boolean isGetService = inputStream.readBoolean();
                //查询服务
                if (isGetService) {
                    String serviceName = inputStream.readUTF();
                    //取出服务提供者的地址集合
                    Set<RegisterVO> voSet = getService(serviceName);
                    //返回给客户端
                    outputStream.writeObject(voSet);
                    outputStream.flush();
                    System.out.println("将查询的服务-" + serviceName + "发送给客户端");
                }
                //注册服务
                else {
                    //获取注册的服务名，ip，端口
                    String serviceName = inputStream.readUTF();
                    String host = inputStream.readUTF();
                    int port = inputStream.readInt();
                    //注册中心保存
                    registerService(serviceName, host, port);
                    outputStream.writeBoolean(true);
                    outputStream.flush();
                    System.out.println("服务注册成功：" + serviceName);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void startService() throws IOException {
        ServerSocket serverSocket = new ServerSocket();
        serverSocket.bind(new InetSocketAddress(port));
        System.out.println("注册中心 on:" + port);
        try {
            while (true) {
                executorService.execute(new ServerTask(serverSocket.accept()));
            }
        } finally {
            serverSocket.close();
        }
    }

    //启动注册中心服务
    @PostConstruct
    public void init() {
        this.port = 9001;
        new Thread(() -> {
            try {
                startService();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

}
